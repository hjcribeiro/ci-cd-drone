package tests;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.junit.runners.SerenityRunner;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import steps.NewCarSearchSteps;
import net.thucydides.core.annotations.Steps;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SerenityRunner.class)
public class NewCarSearchTests {

    private Response res = null;
    private JsonPath jp = null;

    @Steps
    NewCarSearchSteps newCarSearchSteps;


    @Test
    public void NewCarSearchT01VehicleDetailsSuccessResponse(){

        newCarSearchSteps.searchNewCarByRegNo("PJ53OTP");
        newCarSearchSteps.searchIsExecutedSuccesfuly();
        newCarSearchSteps.iShouldFindName("chevrolet chevelle malibu");

    }

    @Test
    public void NewCarSearchT02VehicleDetailsNotFound(){
        newCarSearchSteps.searchNewCarByRegNo("PJ53OTQ");
        newCarSearchSteps.newCarNotFound();
    }
}