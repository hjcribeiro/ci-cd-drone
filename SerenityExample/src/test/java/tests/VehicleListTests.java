package tests;

import net.serenitybdd.junit.runners.SerenityRunner;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import net.thucydides.core.annotations.Steps;
import steps.VehicleListSteps;



@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SerenityRunner.class)
public class VehicleListTests {



    @Steps
    VehicleListSteps vehicleListSteps;

    @Test
    public void VehicleListT00_GetTheListOfAllCars() {
        vehicleListSteps.listAllVehiclesByVehicleType("car");
        vehicleListSteps.searchIsExecutedSuccesfuly();
        vehicleListSteps.iShouldFindListWithNumberOfRecords(406);
    }




    @Test
    public void VehicleListT01_GetVehicleTypeNotFound(){
        vehicleListSteps.listAllVehiclesByVehicleType("boat");
        vehicleListSteps.listOfVehiclesNotFound();
    }
}