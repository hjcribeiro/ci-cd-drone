package steps;

import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import static io.restassured.RestAssured.*;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.Matchers.is;

public class NewCarSearchSteps {
    private String NEW_CAR_SEARCH = "/new-car/details";
    private String BASE_PATH = "/vehicles";
    private String BASE_URI = "http://localhost";


    private Response response;


    @Step("I try to search new car by {} reg No")
    public void searchNewCarByRegNo(String regNo){
        defaultParser = Parser.JSON;
        port = 8080;
        baseURI = BASE_URI;
        basePath = BASE_PATH;
        response = SerenityRest.
                given().param("regNo", regNo).
                when().get(NEW_CAR_SEARCH);
    }

    @Step
    public void searchIsExecutedSuccesfuly(){
        response.then().statusCode(200);
    }

    @Step
    public void newCarNotFound(){
        response.then().statusCode(404);
    }

    @Step
    public void iShouldFindName(String Name){
        response.then().body("Name", is(Name));
    }
}
