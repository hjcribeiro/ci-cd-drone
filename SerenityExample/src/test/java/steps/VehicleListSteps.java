package steps;

import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import static io.restassured.RestAssured.*;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.Matchers.equalTo;

public class VehicleListSteps {
    private String VEHICLE_LIST = "/list-all";
    private String BASE_PATH = "/vehicles";
    private String BASE_URI = "http://localhost";


    private Response response;


    @Step("I try to list all vehicles that are type {}")
    public void listAllVehiclesByVehicleType(String vehicleType){
        defaultParser = Parser.JSON;
        port = 8080;
        baseURI = BASE_URI;
        basePath = BASE_PATH;
        response = SerenityRest.
                given().param("vehicle_type", vehicleType).
                when().get(VEHICLE_LIST);
    }

    @Step
    public void searchIsExecutedSuccesfuly(){
        response.then().statusCode(200);
    }

    @Step
    public void listOfVehiclesNotFound(){
        response.then().statusCode(404);
    }

    @Step
    public void iShouldFindListWithNumberOfRecords(int num){
        response.then().body("size()", equalTo(num));
    }
}
