import json
import requests
import os
from ..main import app
from flask import request, make_response, jsonify

ENV_API_KEY = os.environ['ENV_API_KEY']
ENV_CAZANA_URI = os.environ['ENV_CAZANA_URI']

@app.route("/")
def no_route():
    reg = request.args.get('reg')
    mileage = request.args.get('mileage')
    date = request.args.get('date')
 
    try:
        params = {}
        params['key'] = ENV_API_KEY

        if mileage:
            params['mileage'] = mileage

        if date:
            params['date'] = date

        r = requests.get("{}/vehicle/{}/valuation".format(ENV_CAZANA_URI, reg.upper()), params=params)

        response = make_response(jsonify(r.json()), r.status_code)
        return response

    except requests.exceptions.RequestException as re:  
        response = make_response(jsonify({'error' : str(re)}), 503)
        return response
    except Exception as e:
        response = make_response(jsonify({'error' : str(e)}), 500)
        return response