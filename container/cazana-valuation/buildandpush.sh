#!/bin/sh

if [[ $# -eq 0 ]]
then
    echo 'Missing tag'
    exit 1
fi

tag=$1

echo Using tag: ${tag}

docker build -t cdp-cazana-valuation .
docker tag cdp-cazana-valuation:latest 879461422967.dkr.ecr.eu-west-1.amazonaws.com/cdp-cazana-valuation:${tag}
docker push 879461422967.dkr.ecr.eu-west-1.amazonaws.com/cdp-cazana-valuation:${tag}
