import json
import requests
import os
from ..main import app
from flask import request, make_response, jsonify

API_KEY = os.environ['API_KEY']

@app.route("/")
def no_route():
    reg = request.args.get('reg')
    mileage = request.args.get('mileage')
 
    try:
        params = {}
        params['key'] = API_KEY
        if mileage:
            params['mileage'] = mileage
        r = requests.get("https://api.cazana.com/vehicle/{}/valuation".format(reg.upper()), params=params)

        response = make_response(jsonify(r.json()), r.status_code)
        return response

    except requests.exceptions.RequestException as re:  
        response = make_response(jsonify({'error' : re.message}), 503)
        return response
    except Exception as e:
        response = make_response(jsonify({'error' : str(e)}), 500)
        return response