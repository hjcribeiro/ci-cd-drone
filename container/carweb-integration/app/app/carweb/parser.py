import time
import json
import sys
import requests
import untangle
from datetime import datetime
import traceback
import os


def create_getter_for_vehicle(vehicle):
    def _get(attr_name, default=None):
        """
        Get a named attribute from an object; multi_getattr(x, 'a.b.c.d') is
        equivalent to x.a.b.c.d. When a default argument is given, it is
        returned when any attribute in the chain doesn't exist; without
        it, an exception is raised when a missing attribute is encountered.
        """
        try:
            attr = getattr(vehicle, attr_name).cdata
        except IndexError:
            return default
        return attr
    return _get

def is_car(vehicle, regnum):
    # We use the CAP Code to identify vehicles that are not cars. For cars,
    # characters 18-20 of the Cap Code should be blank.
    MAX_VALID_CAP_CODE_LENGTH_FOR_CARS = 17
    try:
        # Carweb returns multiple CAP IDs for a vehicle. We are only looking at the first one.
        for cap_id_item in vehicle.CapIDs.CapIDItem:
            return len(cap_id_item.CapCode.cdata.strip()) <= MAX_VALID_CAP_CODE_LENGTH_FOR_CARS
    except:
        #logger.error('Carweb did not return a Cap Code for registration number %s' % regnum)
        print('Carweb did not return a Cap Code for registration number {}'.format(regnum))
        return False


def parse_vehicle_data(data):
    """Parse the vehicle data (XML) returned by CarWeb, extracting the image URL or checking that one wasn't
    returned"""
    obj = untangle.parse(data)  # parses the xml from Carweb, converting to a Python object
    try:
        obj = obj.GetVehicles.DataArea.Vehicles.Vehicle
        getter = create_getter_for_vehicle(obj)

        date_first_registered = getter('DateFirstRegistered')
        year = datetime.strptime(date_first_registered, '%Y-%m-%d').year if date_first_registered is not None else None

        vehicle = {
            'make': getter('Combined_Make'),
            'model': getter('Combined_Model'),
            'engineSize': getter('Combined_EngineCapacity'),
            'year': year,
            'transmission': getter('Combined_Transmission'),
            'registrationNumber': getter('VRM_Curr'),
            'imageUrl': getter('VehicleImageUrl')
        }

        return vehicle

    except Exception:       
        return None







