import json
import time
import requests
import os
from ..main import app
from app.carweb.parser import parse_vehicle_data

from flask import request, make_response, jsonify

CACHE_EXPIRE = 60 * 60 * 1

ENV_ENVIRONMENT = os.environ['ENV_ENVIRONMENT']
ENV_USERNAME = os.environ['ENV_USERNAME']
ENV_PASSWORD = os.environ['ENV_PASSWORD']
ENV_CLIENT_REF = os.environ['ENV_CLIENT_REF']
ENV_CLIENT_DESCRIPTION = os.environ['ENV_CLIENT_DESCRIPTION']
ENV_KEY1 = os.environ['ENV_KEY1']
ENV_VERSION = os.environ['ENV_VERSION']
ENV_API_ROOT = os.environ['ENV_API_ROOT']

invoke_params = {
    'strUserName': ENV_USERNAME,
    'strPassword': ENV_PASSWORD,
    'strClientRef': ENV_CLIENT_REF,
    'strClientDescription': ENV_CLIENT_DESCRIPTION,
    'strKey1': ENV_KEY1,
    'strVersion': ENV_VERSION
}

@app.route("/")
def carweb_lookup():
    reg = request.args.get('reg')
    reg = reg.lower()
   
    try:
        invoke_params['strVRM'] = reg 
        r = requests.get(ENV_API_ROOT, params=invoke_params)
        
        if r.status_code == 200:
            vehicle = parse_vehicle_data(r.text)
            if not vehicle:
                return('', 404)
            else:
                response = make_response(jsonify(vehicle), 200)
                return response
        else:
            return ('', r.status_code)

    except requests.exceptions.RequestException as re:  
        response = make_response(jsonify({'error' : re.message}), 503)
        return response
    except Exception as e:
        response = make_response(jsonify({'error' : str(e)}), 500)
        return response



        
        
